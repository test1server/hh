var express = require("express")
var app = express()
app.use(express.static('static'))
app.set("view engine", "ejs")
path = __dirname + "/templates/"
var views = require('./views')
app.get('/', views.main)
app.get('/list/:id', views.listObjects)
// app.get('/obj/:id/', views.arcObject)
app.get('/obj/:id/', views.arcObject)
app.use("*", views.error404)
var port = process.env.PORT || 3000;
app.listen(port, () => {
	console.log("Listening on " + port)
})



